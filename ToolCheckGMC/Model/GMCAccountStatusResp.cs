﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToolCheckGMC.Model
{
    public class GMCAccountStatusResp
    {
        GMCAccountStatusResp()
        {
            Products = new List<Product>();
        }
        public string Kind { get; set; }
        public List<Product> Products { get; set; }
        public string AccountId { get; set; }
        public bool WebsiteClaimed { get; set; }
        public List<AccountLevelIssue> AccountLevelIssues { get; set; }
    }
    public class Product
    {
        Product()
        {
            ItemLevelIssues = new List<ItemLevelIssue>();
        }
        public List<ItemLevelIssue> ItemLevelIssues { get; set; }
        public Statistics Statistics { get; set; }
        public string Destination { get; set; }
        public string Channel { get; set; }
        public string Country { get; set; }
    }
    public class ItemLevelIssue
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public string Servability { get; set; }
        public string Resolution { get; set; }
        public string Documentation { get; set; }
        public string Detail { get; set; }
        public int NumItems { get; set; }
    }
    public class AccountLevelIssue
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Severity { get; set; }
        public string Documentation { get; set; }
    }
    public class Statistics
    {
        public int Active { get; set; }
        public int Pending { get; set; }
        public int Disapproved { get; set; }
        public int Expiring  { get; set; }
    }
}
