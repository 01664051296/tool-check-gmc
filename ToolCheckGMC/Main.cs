﻿using Google.Apis.Auth.OAuth2;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;
using ShopifySharp;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ToolCheckGMC.Model;
using ToolCheckGMC.ServiceShopbase;
using ToolCheckGMC.ServiceShopbase.ResponseEntities;
using ToolCheckGMC.ShopbaseService.ResponseEntities;

namespace ToolCheckGMC
{
    public partial class Main : Form
    {
        MerchantCenterEntities db = new MerchantCenterEntities();
        public Main()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            loadDataGridAccountGMC();
            //loadDataGridShopbaseStores();
        }
        #region tab merchant center
        private void loadDataGridAccountGMC(string keySearch = null)
        {
            //Prepare grid view
            dataGridView1.ColumnCount = 9;
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridView1.Columns[0].Name = "CN";
            dataGridView1.Columns[1].Name = "Merchant ID";
            dataGridView1.Columns[2].Name = "Store Name";
            dataGridView1.Columns[3].Name = "Note";
            dataGridView1.Columns[4].Name = "Update Time";
            dataGridView1.Columns[5].Name = "Total Active";
            dataGridView1.Columns[6].Name = "Total Pending";
            dataGridView1.Columns[7].Name = "Total Disapproved";
            dataGridView1.Columns[8].Name = "Is Active";
            dataGridView1.Rows.Clear();
            var allAccount = db.Accounts.OrderBy(p => p.Id).Where(p => !p.IsDeleted);
            if (!string.IsNullOrEmpty(keySearch))
                allAccount = allAccount.Where(p => p.StoreName.ToLower().Contains(keySearch) || p.Note.ToLower().Contains(keySearch));
            int cardinalNumbers = 1;
            foreach (var item in allAccount)
            {
                string[] row = new string[] { };
                row = new string[] { cardinalNumbers.ToString(), item.MerchantId.ToString(), item.StoreName, item.Note, item.ModificationTime.HasValue ? item.ModificationTime.Value.ToString("dd/MM/yyyy  HH:mm:ss") : "Not update", item.TotalActive.ToString(), item.TotalPending.ToString(), item.TotalDisapproved.ToString(), item.IsActive ? "Active" : "Disapproved" };
                cardinalNumbers++;
                dataGridView1.Rows.Add(row);
            }
            DataGridViewButtonColumn btnCheckGMC = new DataGridViewButtonColumn();
            dataGridView1.Columns.Add(btnCheckGMC);
            btnCheckGMC.HeaderText = "Click To Check";
            btnCheckGMC.Text = "Check";
            btnCheckGMC.Name = "btnCheckGMC";
            btnCheckGMC.UseColumnTextForButtonValue = true;
            DataGridViewButtonColumn btnDelete = new DataGridViewButtonColumn();
            dataGridView1.Columns.Add(btnDelete);
            btnDelete.HeaderText = "Click To Delete";
            btnDelete.Text = "Delete";
            btnDelete.Name = "btnDelete";
            btnDelete.UseColumnTextForButtonValue = true;
        }
        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            //loadDataGridAccountGMC(txtSearch.Text.Trim().ToLower());
        }
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 9)
            {
                var id = Convert.ToInt64(dataGridView1.Rows[e.RowIndex].Cells[1].FormattedValue.ToString());
                var accountUpdated = CheckAccountStatus(db.Accounts.FirstOrDefault(p => p.MerchantId == id));
                db.SaveChanges();
                updateDataView(accountUpdated);
            }
            if (e.ColumnIndex == 10)
            {
                var id = Convert.ToInt64(dataGridView1.Rows[e.RowIndex].Cells[1].FormattedValue.ToString());
                DialogResult dr = MessageBox.Show($"Do you want delete this account \"{id}\"?", "Notification", MessageBoxButtons.YesNo);
                switch (dr)
                {
                    case DialogResult.Yes:
                        db.Accounts.Remove(db.Accounts.FirstOrDefault(p => p.MerchantId == id));
                        db.SaveChanges();
                        break;
                    case DialogResult.No:
                        break;
                }
                loadDataGridAccountGMC(txtSearch.Text.Trim().ToLower());
            }
        }
        protected async void updateDataView(Account account)
        {
            try
            {
                //Loop over rows
                for (int i = 0; i < dataGridView1.Rows.Count; i++)
                {
                    if (dataGridView1.Rows[i].Cells["Merchant ID"].Value.ToString() == account.MerchantId.ToString())
                    {
                        dataGridView1.Rows[i].Cells["Merchant ID"].Value = account.MerchantId.ToString();
                        dataGridView1.Rows[i].Cells["Store Name"].Value = account.StoreName.ToString();
                        dataGridView1.Rows[i].Cells["Note"].Value = account.Note.ToString();
                        dataGridView1.Rows[i].Cells["Update Time"].Value = account.ModificationTime.Value.ToString("dd/MM/yyyy  HH:mm:ss");
                        dataGridView1.Rows[i].Cells["Total Active"].Value = account.TotalActive.ToString();
                        dataGridView1.Rows[i].Cells["Total Pending"].Value = account.TotalPending.ToString();
                        dataGridView1.Rows[i].Cells["Total Disapproved"].Value = account.TotalDisapproved.ToString();
                        dataGridView1.Rows[i].Cells["Is Active"].Value = account.IsActive ? "Active" : "Disapproved";
                        break;

                    } 
     
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
            finally
            {

            }
        }
        public Account CheckAccountStatus(Account item)
        {

            string GOOGLE_CONTENT_API_SCOPE = "https://www.googleapis.com/auth/content";
            try
            {
                var token = GoogleCredential
                                           .FromJson(item.Json) // Loads key file
                                           .CreateScoped(GOOGLE_CONTENT_API_SCOPE) // Gathers scopes requested
                                           .UnderlyingCredential // Gets the credentials
                                           .GetAccessTokenForRequestAsync().Result; // Gets the Access Token

                RestClient client = new RestClient($"https://shoppingcontent.googleapis.com/content/v2.1/{item.MerchantId}/accountstatuses/{item.MerchantId}");
                RestRequest getRequest = new RestRequest(Method.GET);
                getRequest.AddHeader("Accept", "application/json");
                getRequest.AddHeader("authorization", "Bearer " + token);

                IRestResponse getResponse = client.Execute(getRequest);

                var statusResp = JsonConvert.DeserializeObject<GMCAccountStatusResp>(getResponse.Content);
                item.TotalActive = 0;
                item.TotalPending = 0;
                item.TotalDisapproved = 0;
                item.TotalExpiring = 0;
                //prepare content response
                string content = $"***** \"{item.StoreName}\": google merchant statistics today! ***** \n";
                content += statusResp.WebsiteClaimed ? "- Verified website." : "- The site has not been authenticated.";
                foreach (var productFeed in statusResp.Products)
                {
                    item.TotalActive += productFeed.Statistics.Active;
                    item.TotalPending += productFeed.Statistics.Pending;
                    item.TotalDisapproved += productFeed.Statistics.Disapproved;
                    item.TotalExpiring += productFeed.Statistics.Expiring;
                    item.IsActive = item.TotalActive > 0 || item.TotalPending > 0;
                    content += $"\n- Country :{productFeed.Country}. \nThere are: \n\t{item.TotalActive} active products. \n\t{item.TotalPending} pending products. \n\t{item.TotalExpiring} expiring products. \n\t{item.TotalDisapproved} disapproved products. \n";
                    content += productFeed.ItemLevelIssues.Any() ? "- Product issues:\n" : "- No problem about the product.\n";
                    int totalIssues = 1;
                    foreach (var itemLevelIssues in productFeed.ItemLevelIssues)
                    {
                        content += $"* Issue {totalIssues}: {itemLevelIssues.Description}.\n\t- Servability: {itemLevelIssues.Servability}. \n\t- Number of products affected {itemLevelIssues.NumItems}. \n\t- Detail: {itemLevelIssues.Detail}.\n\t- Documentation: {itemLevelIssues.Documentation} \n";
                        totalIssues++;
                    }
                }
                content += "\n\n----------------------------RESPONSE WITH JSON CONTENT----------------------------\n\n" + getResponse.Content;
                item.ModificationTime = DateTime.Now;

                rtbResponse.Text = content;
                rtbResponse.SelectionStart = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Response", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            return item;
        }
        private void btnAddGMC_Click(object sender, EventArgs e)
        {
            AddNewGMC popup = new AddNewGMC();
            DialogResult dialogresult = popup.ShowDialog();
            loadDataGridAccountGMC(txtSearch.Text.Trim().ToLower());
            popup.Dispose();
        }

        #endregion

        private void btnAddAList_Click(object sender, EventArgs e)
        {
            AddAListGMC popup = new AddAListGMC();
            DialogResult dialogresult = popup.ShowDialog();
            loadDataGridAccountGMC(txtSearch.Text.Trim().ToLower());
            popup.Dispose();
        }

        private void txtSearch_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (!txtSearch.AcceptsReturn)
                {
                    loadDataGridAccountGMC(txtSearch.Text.Trim().ToLower());
                }
            }
        }
    }
}
