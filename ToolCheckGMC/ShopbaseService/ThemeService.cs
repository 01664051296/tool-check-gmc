﻿using ShopifySharp.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using ToolCheckGMC.ServiceShopbase.ResponseEntities;
using ToolCheckGMC.ShopbaseService.ResponseEntities;

namespace ToolCheckGMC.ServiceShopbase
{
    public class ThemeService: ShopBaseService
    {
        public ThemeService(string onShopBaseUrl, string shopAccessToken, string shopApiKey) : base(onShopBaseUrl, shopAccessToken, shopApiKey) { }
        public virtual async Task<List<PageShopbase>> GetPages()
        {
            var request = PrepareRequest("pages.json");
            return await ExecuteRequestAsync<List<PageShopbase>>(request, HttpMethod.Get, rootElement: "pages");
        }
        public virtual async Task<PageShopbase> GetPage(long pageId)
        {
            var request = PrepareRequest($"pages/{pageId}.json");
            return await ExecuteRequestAsync<PageShopbase>(request, HttpMethod.Get, rootElement: "page");
        }
        public virtual async Task<PageShopbase> UpdatePage(PageShopbase page)
        {
            var request = PrepareRequest($"pages/{page.Id}.json");
            var content = new JsonContent(new
            {
                page = page
            });
            return await ExecuteRequestAsync<PageShopbase>(request, HttpMethod.Put, content, rootElement: "page");
        }
        public virtual async Task<PageShopbase> CreatePage(PageShopbase page)
        {
            var request = PrepareRequest("pages.json");

            var content = new JsonContent(new
            {
                page = page
            });
            return await ExecuteRequestAsync<PageShopbase>(request, HttpMethod.Post, content, "page");
        }


        public virtual async Task<List<NavigationMenu>> GetNavigationMenus()
        {
            var request = PrepareRequest("menus.json");
            return await ExecuteRequestAsync<List<NavigationMenu>>(request, HttpMethod.Get, rootElement: "menus");
        }
        public virtual async Task<List<Collection>> GetAllCollections()
        {
            var request = PrepareRequest("collections.json");
            return await ExecuteRequestAsync<List<Collection>>(request, HttpMethod.Get, rootElement: "collections");
        }
        public virtual async Task<Collection> CreateCollection(Collection collection)
        {
            var request = PrepareRequest("smart_collections.json");
            var content = new JsonContent(new
            {
                smart_collection = collection
            });
            return await ExecuteRequestAsync<Collection>(request, HttpMethod.Post, content, "smart_collection");
        }
        public virtual async Task<Collection> UpdateCollection(Collection collection)
        {
            var request = PrepareRequest($"smart_collections/{collection.Id}.json");
            var content = new JsonContent(new
            {
                smart_collection = collection
            });
            return await ExecuteRequestAsync<Collection>(request, HttpMethod.Put, content, "smart_collection");
        }
    }
}
