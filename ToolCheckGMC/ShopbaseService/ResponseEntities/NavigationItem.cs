﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToolCheckGMC.ShopbaseService.ResponseEntities
{
    public class NavigationItem
    {
        [JsonProperty("id")]
        public long Id { get; set; }
        [JsonProperty("menu_id")]
        public long MenuId { get; set; }
        [JsonProperty("shop_id")]
        public string ShopId { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("type")]
        public string Type { get; set; }
        [JsonProperty("type_options")]
        public string TypeOptions { get; set; }
    }
}
