﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToolCheckGMC.ServiceShopbase.ResponseEntities
{
    public class Legal
    {
        [JsonProperty("refund_policy")]
        public string RefundPolicy { get; set; }
        [JsonProperty("privacy_policy")]
        public string PrivacyPolicy { get; set; }
        [JsonProperty("shipping_policy")]
        public string ShippingPolicy { get; set; }
        [JsonProperty("terms_of_service")]
        public string TermsOfService { get; set; }
    }
}
