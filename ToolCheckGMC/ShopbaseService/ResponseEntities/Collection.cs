﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToolCheckGMC.ShopbaseService.ResponseEntities
{
    public class Collection
    {
        [JsonProperty("id")]
        public long Id { get; set; }
        [JsonProperty("collection_type")]
        public string Type { get; set; }
        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("disjunctive")]
        public bool Disjunctive { get; set; }
        [JsonProperty("body_html")]
        public string BodyHtml { get; set; }
        [JsonProperty("rules")]
        public List<Rule> Rules { get; set; }
        [JsonProperty("metafields")]
        public List<Metafield> Metafields { get; set; }
       
    }
    public enum CollectionType {
        smart
    }
    public class Rule
    {
        [JsonProperty("column")]
        public string Column { get; set; }
        [JsonProperty("relation")]
        public string Relation { get; set; }
        [JsonProperty("condition")]
        public string Condition { get; set; }
    }
    public class Metafield
    {
        [JsonProperty("namespace")]
        public string Namespace { get; set; }
        [JsonProperty("key")]
        public string Key { get; set; }
        [JsonProperty("value")]
        public string Value { get; set; }
        [JsonProperty("value_type")]
        public string ValueType { get; set; }
    }
}
