﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToolCheckGMC.ShopbaseService.ResponseEntities
{
    public class NavigationMenu
    {
        [JsonProperty("id")]
        public long Id { get; set; }
        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("shop_id")]
        public string ShopId { get; set; }
    }
}
