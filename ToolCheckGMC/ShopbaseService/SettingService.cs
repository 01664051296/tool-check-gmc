﻿using RestSharp;
using ShopifySharp;
using ShopifySharp.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using ToolCheckGMC.ServiceShopbase.ResponseEntities;

namespace ToolCheckGMC.ServiceShopbase
{
    public class SettingService: ShopBaseService
    {
        public SettingService(string onShopBaseUrl, string shopAccessToken, string shopApiKey) : base(onShopBaseUrl, shopAccessToken, shopApiKey) { }
        [Obsolete("This ListAsync method targets a version of Shopify's API which will be deprecated and cease to function in April of 2020. ShopifySharp version 5.0 will be published soon with support for the newer list API. Make sure you update before April of 2020.")]
        public virtual async Task<Legal> UpdateLegal(Legal legal)
        {
            var request = PrepareRequest("setting/legal.json");

            return await ExecuteRequestAsync<Legal>(request, HttpMethod.Put, rootElement: "legal");
        }
        public virtual async Task<Legal> GetLegal()
        {
            var request = PrepareRequest("setting/legal.json");
            return await ExecuteRequestAsync<Legal>(request, HttpMethod.Get, rootElement: "legal");
        }
       
    }
}
