﻿
namespace ToolCheckGMC
{
    partial class AddAListGMC
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rtbContentListGMC = new System.Windows.Forms.RichTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnSubmitAddAList = new System.Windows.Forms.Button();
            this.lblLogImportAListGMC = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // rtbContentListGMC
            // 
            this.rtbContentListGMC.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtbContentListGMC.Location = new System.Drawing.Point(12, 44);
            this.rtbContentListGMC.Name = "rtbContentListGMC";
            this.rtbContentListGMC.Size = new System.Drawing.Size(776, 332);
            this.rtbContentListGMC.TabIndex = 0;
            this.rtbContentListGMC.Text = "";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(354, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Format Content: Store Name | Merchant ID | Note | Link Json Dropbox File";
            // 
            // btnSubmitAddAList
            // 
            this.btnSubmitAddAList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSubmitAddAList.Location = new System.Drawing.Point(694, 395);
            this.btnSubmitAddAList.Name = "btnSubmitAddAList";
            this.btnSubmitAddAList.Size = new System.Drawing.Size(75, 23);
            this.btnSubmitAddAList.TabIndex = 3;
            this.btnSubmitAddAList.Text = "Add List";
            this.btnSubmitAddAList.UseVisualStyleBackColor = true;
            this.btnSubmitAddAList.Click += new System.EventHandler(this.btnSubmitAddAList_Click);
            // 
            // lblLogImportAListGMC
            // 
            this.lblLogImportAListGMC.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblLogImportAListGMC.AutoSize = true;
            this.lblLogImportAListGMC.Location = new System.Drawing.Point(12, 395);
            this.lblLogImportAListGMC.Name = "lblLogImportAListGMC";
            this.lblLogImportAListGMC.Size = new System.Drawing.Size(36, 13);
            this.lblLogImportAListGMC.TabIndex = 4;
            this.lblLogImportAListGMC.Text = "Logs: ";
            // 
            // AddAListGMC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.lblLogImportAListGMC);
            this.Controls.Add(this.btnSubmitAddAList);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.rtbContentListGMC);
            this.Name = "AddAListGMC";
            this.Text = "AddAList";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox rtbContentListGMC;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnSubmitAddAList;
        private System.Windows.Forms.Label lblLogImportAListGMC;
    }
}