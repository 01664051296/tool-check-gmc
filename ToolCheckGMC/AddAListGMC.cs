﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ToolCheckGMC
{
    public partial class AddAListGMC : Form
    {
        public AddAListGMC()
        {
            InitializeComponent();
        }

        private async void btnSubmitAddAList_Click(object sender, EventArgs e)
        {
            int imported = 0;
            try
            {
                var lines = rtbContentListGMC.Lines;
                lblLogImportAListGMC.Text = "Importing...";
                await Task.Run(() =>
                {
                    List<Exception> errorLines = new List<Exception>();
                    //open db connection
                    foreach (var line in lines)
                    {
                        string[] args = line.Split('|');
                        try
                        {
                            string gmcName = "";
                            string gmcId = "";
                            string note = "";
                            string linkJson = "";
                            if (!args.Any())
                                throw new Exception($"Invalid line data {line}");
                            gmcName = args[0];
                            if (string.IsNullOrEmpty(gmcName))
                                throw new Exception("GMC name data is not found");

                            gmcId = args[1];
                            if (!gmcId.Any())
                                throw new Exception("GMC ID data is not found");
                            note = args[2];
                            linkJson = args[3];
                            if (string.IsNullOrEmpty(linkJson))
                                throw new Exception("Link json data is not found");

                            //create pre import gmc
                            string jsonText = System.IO.File.ReadAllText(linkJson);

                            using (MerchantCenterEntities db = new MerchantCenterEntities())
                            {
                                var idMerchant = long.Parse(gmcId);
                                if (db.Accounts.Where(p => p.MerchantId == idMerchant).Any())
                                {
                                    throw new Exception("Account already exists");
                                }
                                var newAccount = new Account
                                {
                                    MerchantId = idMerchant,
                                    Note = note,
                                    StoreName = gmcName,
                                    Json = jsonText,
                                    CreationTime = DateTime.Now,
                                    IsActive = true,
                                    TotalActive = 0,
                                    TotalDisapproved = 0,
                                    TotalExpiring = 0,
                                    TotalPending = 0,
                                };
                                db.Accounts.Add(newAccount);
                                db.SaveChanges();
                                imported++;
                            }

                        }
                        catch (Exception ex)
                        {
                            Invoke(new Action(() =>
                            {
                                ex.Source = line;
                                errorLines.Add(ex);
                            }));
                        }

                    }
                    if (errorLines.Any())
                    {
                        Invoke(new Action(() =>
                        {
                            rtbContentListGMC.AppendText(Environment.NewLine);
                            rtbContentListGMC.AppendText("******ERROR LINES******");
                            rtbContentListGMC.AppendText(Environment.NewLine);
                            foreach (var ex in errorLines)
                            {
                                rtbContentListGMC.AppendText($"{ex.Source} => Error: {ex.Message}");
                                rtbContentListGMC.AppendText(Environment.NewLine);
                            }
                            rtbContentListGMC.AppendText("***********************");

                        }));
                    }
                });
            }
            catch (Exception ex)
            {
                lblLogImportAListGMC.Text = "Import failed";
                MessageBox.Show($"An error occured: {ex.Message}", "Import error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                lblLogImportAListGMC.Text = $"Imported {imported} gmc";
                lblLogImportAListGMC.Text = "Imported Completed";
            }
        }
    }
}
